package co.com.choucair.certification.proyectobase.userinterface;

import net.serenitybdd.screenplay.targets.Target;
import org.openqa.selenium.By;

public class SearchCoursePage {
    public static final Target BUTTON_UC = Target.the("selecciona cursos y certificados").located(By.xpath("//div[@id='certificaciones']//strong"));
    public static final Target INPUT_COURSE = Target.the("buscar el curso").located(By.id("coursesearchbox"));
    public static final Target BUTTON_GO = Target.the("da click para buscar el curso").located(By.xpath("//button[@class='btn btn-secondary']"));
    public static final Target SELECT_COURSE = Target.the("da click para buscar el curso").located(By.xpath("//a[contains(text(),'ISTQB Agile Tester Extension')]"));
    public static final Target NAME_COURSE = Target.the("Extrae el nombre del curso").located(By.xpath("//h1[contains(text(),'ISTQB Agile Tester Extension')]"));
}
